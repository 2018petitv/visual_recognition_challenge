import tweepy

# We import our access keys:
from credentials import *


def twitter_setup():
    """
    Utility function to setup the Twitter's API
    with an access keys provided in a file credentials.py
    :return: the authentified API
    """
    # Authentication and access using keys:
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)

    # Return API with authentication:
    api = tweepy.API(auth)
    return api


def collect_image_url(txt):
    """récupère les url d'image de tweets contenant txt, ainsi que le texte du tweet, et l'id de l'utilisateur"""
    liste_data = []
    connexion = twitter_setup()
    tweets = connexion.search(txt, language="english", rpp=100, count=100)

    for tweet in tweets:
        txt=tweet.text
        user=tweet.user
        id=user.id
        entities = tweet.entities
        if 'media' in entities:
            if entities['media'][0]['type'] == 'photo':
                liste_data.append((tweet.entities['media'][0]['media_url'],txt,id))
    return liste_data


def collect_texte(mot):
    """renvoie le premier tweet contenant 1 mot"""
    liste_data = []
    connexion = twitter_setup()
    tweets = connexion.search(mot, language="english", rpp=100, count=100)

    for tweet in tweets:
        if len(tweet.text) < 135 :
            return tweet.text
    if len(tweets) > 0:
        txt = tweets[0].text
        return txt

def collect_text_mots(mot1, mot2):
    """renvoie le premier tweet contenant les 2 mots, ou seulement le premier si il n'y en a pas"""
    liste_data = []
    connexion = twitter_setup()
    tweets = connexion.search(mot1 + ' AND ' + mot2, language="english", rpp=100, count=100)

    # si on ne trouve pas de tweet avec les 2 mot, on ne recherche que le premier
    if len(tweets) == 0:
        return collect_texte(mot1)

    # choisit les tweets qui font moins de 140 caractères, sinon le texte du tweet est coupé
    for tweet in tweets:
        if len(tweet.text) < 135 :
            return tweet.text

    txt = tweets[0].text
    return txt


def vrai_texte(texte_tweet):
    """supprime les informations non-pertinentes du texte du tweet"""
    if texte_tweet[0:2] == 'RT':
        for i in range(len(texte_tweet)):
            if texte_tweet[i] == ':':
                texte_tweet = texte_tweet[i+1:]
                break

    citation = False
    liste_mots = texte_tweet.split(" ")

    for i in range(len(liste_mots)) :
        # supprime les mentions
        if len(liste_mots[i]) > 0 and liste_mots[i][0] == '@':
            liste_mots[i] = ''

        # supprime le lien vers le tweet
        if 'https' in liste_mots[i]:
            liste_mots[i] = ''

    f = fold_string(liste_mots)

    return f

def fold_string(liste):
    """concatène une liste string, et ajoute des espaces entre eux"""

    a = ""
    for mot in liste:
        if mot != "":
            a += mot + " "

    if a=="":
        return a
    return a[:-1]

import requests

def load_requests(source_url, sink_path):
   """
   Load a file from an URL (e.g. http).

   Parameters
   ----------
   source_url : str
       Where to load the file from.
   sink_path : str
       Where the loaded file is stored.
   """

   r = requests.get(source_url, stream=True)
   if r.status_code == 200:
       with open(sink_path, 'wb') as f:
           for chunk in r:
               f.write(chunk)

# je rajoute qqc pour pouvoir commit