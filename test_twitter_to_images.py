import pytest
import visual_recognition_challenge.twitter_to_images as tti


def test_collect_image_url():

    # on teste avec un tweet ayant un message unique, et une image
    image_url, texte, id = tti.collect_image_url("Un_tweet_tout_à_fait_standart_avec_une_image")[0]
    texte = tti.vrai_texte(texte)

    assert image_url == 'http://pbs.twimg.com/media/DsixXJ9XoAkM7LY.png'
    assert texte == "Un_tweet_tout_à_fait_standart_avec_une_image"
    assert id == 1062337333138284544


def test_vrai_texte():
    """test que vrai_texte supprime bien les RTs et les citations"""
    texte = "RT @arnaque : ceci est un test"
    sans_rt = tti.vrai_texte(texte)
    assert sans_rt == 'ceci est un test'

    texte = "@tomate @potato azertyuiop @johncena"
    sans_cit = tti.vrai_texte(texte)

    assert sans_cit == "azertyuiop"

    texte = "RT : ceci est un test avec rt et citation @RT RT: @prout"
    sans_cit_ni_rt = tti.vrai_texte(texte)

    assert sans_cit_ni_rt == 'ceci est un test avec rt et citation RT:'


def test_fold_string():
    """teste le fait que fold string concatène correctement des strings, avec des strings vides et d'autres strings"""

    string_de_liste_vide = tti.fold_string([])
    assert string_de_liste_vide == ""

    string_liste_str_vide = tti.fold_string(["","",""])
    assert string_liste_str_vide == ""

    string = tti.fold_string(['base'])
    assert string == "base"

    string = tti.fold_string(["ceci","","est","un",'test.'])
    assert string == "ceci est un test."


def test_collect_texte():

    # on teste simplement que l'on récupère bien des tweets
    dernier_tweet = tti.collect_texte("a")
    assert dernier_tweet != None

    # comme le contenu des tweet change sans cesse on peut difficilement faire un test certain
    # j'ai posté un tweet qui est le seul à contenir le mot clé, et qui le restera, à moins que qqn cherche volontairement à faire échouer ce test
    texte_tweet_solitaire = tti.collect_texte("ce_tweet_est_le_seul_tweet_à_contenir_ce_mot")
    assert texte_tweet_solitaire == "ce_tweet_est_le_seul_tweet_à_contenir_ce_mot  voici_un_autre_mot pomme"

def test_collect_text_mots():

    # un seul tweet contenant les 2 mots clés
    texte_tweet_solitaire = tti.collect_text_mots("ce_tweet_est_le_seul_tweet_à_contenir_ce_mot", "voici_un_autre_mot")
    assert texte_tweet_solitaire == "ce_tweet_est_le_seul_tweet_à_contenir_ce_mot  voici_un_autre_mot pomme"

    # le tweet est le seul à contenir le premier mot, mais ne continent pas le deuxième
    texte_tweet = tti.collect_text_mots("ce_tweet_est_le_seul_tweet_à_contenir_ce_mot", "un_mot_pas_dans_le_tweet")
    assert texte_tweet == "ce_tweet_est_le_seul_tweet_à_contenir_ce_mot  voici_un_autre_mot pomme"

    # voici_un_autre_mot et azertyuiop sont contenus dans d'autres tweet, mais seul celui là contient les 2
    texte_tweet = tti.collect_text_mots("voici_un_autre_mot", "azertyuiop")
    assert texte_tweet == "voici_un_autre_mot pomme azertyuiop"





