"""DESCRIPTION : Fichier qui s'occupe de l'interface graphique une fois les données récupérées"""


"""On charge tous les modules nécessaires"""
import dash
import flask
import dash_core_components as dcc
import dash_html_components as html
import ImageRec
from ImageRec import string_to_comment


"""FONCTION CENTRALE : Prend en entrée une chaîne de caractères et génère une page sur laquelle on affiche le résultat de la fonction string_to_comment"""
def projet_final(str):
    try:
        (image, url, txt, words)=string_to_comment(str)
    except:
        return
    external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']
    
    app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
    
    #Partie CSS
    colors = {
    'background': '#111111',
    'text': '#7FDBFF'
    }
    
    #Partie HTML

    #Background et titre
    app.layout = html.Div(style={'backgroundColor': colors['background']}, children=[
    html.H1(
        children='MVP',
        style={
        'textAlign': 'center',
            'color': colors["text"],
            'position': 'absolute',
            'width':'600px',
            'top': '10px',
            'left': '600px',
        }
    ),

    #Sous-titre
    html.H2(children='Génerer des commentaires de tweets à partir de leur image', style={
        'textAlign': 'center',
            'color': colors["text"],
            'position': 'absolute',
            'width':'600px',
            'top': '70px',
            'left': '600px',

    }),

    #dimensionnement de l'image et donc de la fenêtre d'affichage
    html.Img(src=url,width=550,height=600),

    #Résultat de la reconnaissance d'image
    html.H4(
        children='La reconnaissance d\'image a identifié comme mot clé : ' + words,
        style={
        'textAlign': 'center',
            'color': '#A6A4D8',
            'position': 'absolute',
            'width':'600px',
            'top': '200px',
            'left': '600px',
        }
    ),

    #Commentaire pertinent associé à l'image trouvée sur Twitter
    html.H4(
        children='Le commentaire généré est : ' + txt,
        style={
        'textAlign': 'center',
            'color': '#EFB24E',
            'position': 'absolute',
            'width':'600px',
            'top': '300px',
            'left': '600px',
        }
    ),
    
    ])
    
    app.run_server(port=8000,debug=False)
    #Le résultat est visualisé en entrant l'adresse suivante dans son navigateur : http://127.0.0.1:8000/