"""DESCRIPTION : Script qui s'occupe de la récupération et du traitement des données"""


"""On charge tous les modules nécessaires"""
from imageai.Prediction import ImagePrediction
import os
import requests
import tweepy
import random
import string
from credentials import *
from textblob import TextBlob
from PIL import  Image
from vocabulary.vocabulary import Vocabulary as vb
from selenium import webdriver
from bs4 import BeautifulSoup
from selenium.webdriver.chrome.options import Options


"""On donne à imageai la base de données d'image utilisée comme référence"""
execution_path = os.getcwd()
prediction = ImagePrediction()
prediction.setModelTypeAsResNet()
prediction.setModelPath(os.path.join(execution_path, "resnet50_weights_tf_dim_ordering_tf_kernels.h5"))
prediction.loadModel()


"""Prend en entrée le nom d'une image jpg située au même endroit que le fichier python, et retourne un mot-clé correspondant à la reconnaissance d'image"""
def predict(str):
    predictions, probabilities = prediction.predictImage(os.path.join(execution_path, str), result_count=5 )
    return(predictions[0])


"""Prend en entrée une url et un chemin sur l'ordinateur, et enregistre l'image associée à l'url à l'endroit indiqué"""
def save_image(source_url, sink_path):
    r = requests.get(source_url, stream=True)
    if r.status_code == 200:
        with open(sink_path, 'wb') as f:
            for chunk in r:
                f.write(chunk)


"""Prend un entier N en entrée, et retourne un nom d'image aléatoire à N caractères contenant des lettres majuscules et des chiffres"""
def name(N):
    return(''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(N)))


"""Fonction utilitaire que met en place l'API Twitter avec les clés d'accès du fichier credentials.py et qui retourne API authentifiée"""
def twitter_setup():
    # Authentication and access using keys:
    auth = tweepy.OAuthHandler(CONSUMER_KEY, CONSUMER_SECRET)
    auth.set_access_token(ACCESS_TOKEN, ACCESS_SECRET)
    # Return API with authentication:
    api = tweepy.API(auth)
    return api


"""Prend en entrée une chaîne de caractères, et revoie une liste d'urls d'images trouvés dans des Tweets récents associés à la chaîne de caractères entrée"""
def collect_image_url(txt):
    liste_url=[]
    connexion = twitter_setup()
    tweets = connexion.search(txt, language="english", rpp=20, count=20)
    for tweet in tweets:
        entities=tweet.entities
        if 'media' in entities:
            if entities['media'][0]['type']=='photo':
                liste_url.append(tweet.entities['media'][0]['media_url'])
    return liste_url


"""Prend en entrée une chaîne de caractères, collecte des images sur Twitter associées à cette chaîne, enregistre les images dans un dossier en leur donnant un nom aléatoire, puis analyse chaque image pour trouver un mot-clé correspondant. La fonction renvoie une liste de triplet contenant : le nom de l'image, le mot issu de la reconnaissance d'image, l'url de l'image originale"""
def text_to_keyword(str):
    urls=collect_image_url(str)
    image_list=[]
    for url in urls:
        nom=name(3)
        save_image(url,nom + ".jpg")
        keyword=predict(nom + ".jpg")
        image_list.append((nom,keyword,url))
    return(image_list)


"""FONCTION CENTRALE : Prend en entrée une chaîne de caractères, collecte une image sur Twitter associée à cette chaîne, enregistre l'images dans un dossier en lui donnant un nom aléatoire, puis analyse l'image pour trouver un mot-clé correspondant, et enfin génère un commentaire pertinent à l'aide de la fonction generate_com. La fonction renvoie un quadruplet contenant : le nom de l'image,  l'url de l'image originale, un commentaire pertinent, le mot issu de la reconnaissance d'image"""
def string_to_comment(str):
   text=""
   while len(text)==0:
       try:
           (image, pred, url) = text_to_keyword(str)[0]
       except:
           print("Aucune image correspondant à la recherche trouvée sur Twitter... :(")
       pred = pred.replace("_"," ")
       print(pred)
       text=generate_com(pred)
       text = text.replace(":"," ")
   return (image,url,text,pred)


   """Fonction qui suprimme tout les caractères spéciaux "\"[]:\\"
   :param text: String contenant le texte générer par Vocabulary. Le texte contient plusieurs caractère spéciaux.
   :return: String du texte sans les caractères spécieaux"""
def separator(text):
   interdits= "\"[]:\\"
   sentences=[]
   i=0
   insent=False
   while i<len(text):
          if insent:
              if text[i] == '}':
                  insent=False
              elif text[i] not in interdits:
                  sentences[-1] += text[i]
          elif text[i] == '{':
              insent=True
              sentences.append('')
          i+=1
   return [s[12:] for s in sentences]


   """Fonction qui utlise la bibliothèque Vocabulary basée sur le site Urbain Dictionary pour générer des phrases à partir d'un mot.
   :param word: Le mot clé correspondent à l'image analysée.
   :return: une liste de strings contenant des exemples a partir du mot clé."""
def example_sentences(word):
   return separator(vb.usage_example(word))


"""Fonction qui sélectionne une phrase parmi une liste de phrase.
   :param word: Le mot clé correspondent à l'image analysée.
   :return: Une phrase avec une positivé élevée."""
def select_by_positivity(word):
   sents=example_sentences(word)
   s=TextBlob('')
   pos = random.uniform(-0.3, 0.3)
   stopstr="!.?"
   i=0
   for text in sents:
       for sent in TextBlob(text).sentences:
           if sent.sentiment.polarity >= random.uniform(-0.9,0.9):
               s=sent
               pos=sent.sentiment.polarity
   stri = str(s)
   subst =""
   trouver = False
   for st in stri:
       i+=1
       for sp in stopstr:
           if st == sp:
               subst= stri[:i]
               trouver = True
               break
       if trouver:
           break
   return(subst)


"""Fonction qui fait appelle à la bibliothèque Vocabulary ainsi qu'au site Cambridge Dictonnary afin de génerer une phrase à partir d'un mot.
   :param word: Le mot clé correspondent à l'image analysée.
   :return: un commentaire en rapport avec le mot clé."""
def generate_com(word):
   options = Options()
   options.add_argument('--headless')
   path = r"D:\CentraleSupélec\CW2\chromedriver_win32\chromedriver.exe"
   driver = webdriver.Chrome(path, options=options)
   driver.get("https://dictionary.cambridge.org/dictionary/english/" + word)
   page_source=driver.page_source
   html = BeautifulSoup(page_source,'html.parser')
   sentence = []

   try :
       sentence.append(select_by_positivity(word))
   except:
       pass
   #Cherche un exemple complexe
   for tag in html.find_all('li',class_ ="eg"):
       sentence.append(tag.text)
   #Si on en trouve pas, on prends un exemple plus basique
   for tag in html.find_all('div', class_="examp emphasized"):
       sentence.append(tag.text)
   #Sinon on prend la définition du mot
   if sentence == ['']:
       for tag in html.find_all('b', class_="def"):
           sentence.append(tag.text)

   j=0
   for i in range(0,len(sentence)):
       if sentence[i-j]=='':
           sentence.pop(i)
           j+=1

   com = sentence[random.randint(0, len(sentence)-1)]
   driver.close()
   return com