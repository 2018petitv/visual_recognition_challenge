import tkinter as tk

import tkinter
import visual_recognition_challenge.classify_image as ci




def main(_):
    top = tkinter.Tk()
    # Code to add widgets will go here...

    texte = tk.Label(top, text='Entrez un mot clé puis appuyez sur Valider')
    texte.pack()

    zone_texte = tk.Entry(top)
    zone_texte.pack()

    def valider():
        mot_cle = zone_texte.get()
        bordel = ci.api_final(mot_cle)
        print(bordel)

    bouton_entree = tk.Button(top, text="Valider", command=valider)
    bouton_entree.pack()


    top.mainloop()