import dash
import dash_core_components as dcc
import dash_html_components as html


external_stylesheets = ['https://codepen.io/chriddyp/pen/bWLwgP.css']

app = dash.Dash(__name__, external_stylesheets=external_stylesheets)
with open ("fichier.txt", "r") as f :
    for line in f:
        print (line)
        url,txt,words=line.split(";;;")

colors = {
    'background': '#111111',
    'text': '#7FDBFF'
}

app.layout = html.Div(style={'backgroundColor': colors['background']}, children=[
    html.H1(
        children='MVP',
        style={
            'textAlign': 'center',
            'color': colors['text']
        }
    ),

    html.H2(children='Génerer des commentaires de tweets à partir de leur image', style={
        'textAlign': 'center',
        'color': colors['text']

    }),

    html.Img(src=url,width=200,height=300),

    html.Div(
        children='les mots trouvés sont : ' + words,
        style={
            'position': 'absolute',
            'width':'400px',
            'top': '250px',
            'left': '300px',
            'textAlign': 'center',
            'color': '#00FF00'
        }
    ),

    html.Div(
        children=txt,
        style={
            'textAlign': 'center',
            'color': '#FFFFFF',
            'position': 'absolute',
            'width':'400px',
            'top': '320px',
            'left': '300px',
        }
    ),

])

if __name__ == '__main__':
    app.run_server(debug=True)
